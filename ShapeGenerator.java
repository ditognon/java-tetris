import java.util.ArrayList;
import java.util.Random;

public class ShapeGenerator {
    public ShapeGenerator() {

    }

    public Shape generateRandom() {
        Random rand = new Random();

        int color = rand.nextInt(7) + 1;

        int select = rand.nextInt(7);
        Shape selected;

        switch(select) {
            case 0: selected = new Line();
                    break;
            case 1: selected = new Square();
                    break;
            case 2: selected = new LL();
                    break;
            case 3: selected = new RL();
                    break;
            case 4: selected = new Z();
                    break;
            case 5: selected = new RZ();
                    break;
            case 6: selected = new Arrow();
                    break;
            default: selected = null;
        }

        selected.setColor(color);

        return selected;
    }
}
