public class Arrow extends Shape {
    int position;

    public Arrow() {
        super();

        Point p0 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 0);
        Point p1 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale) + 1, 1);
        Point p2 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 1);
        Point p3 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale) - 1, 1);

        position = 0;

        components.add(p0);
        components.add(p1);
        components.add(p2);
        components.add(p3);
    }

    @Override
    public void rotate() {
        switch(position) {
            case 0: {
                    if(moveableDown()) {
                        components.get(3).setX(components.get(2).getX());
                        components.get(3).setY(components.get(2).getY() + 1);

                        position = 1;
                    }
                    }
                    break;
            case 1: {
                    if(moveableLeft()) {
                        components.get(0).setX(components.get(2).getX() - 1);
                        components.get(0).setY(components.get(2).getY());
                        position = 2;
                    }
                    
                    }
                    break;
            case 2: {
                    if(moveableUp()) {
                        components.get(1).setX(components.get(2).getX());
                        components.get(1).setY(components.get(2).getY() - 1);
                        position = 3;
                    }
                    
                    }
                    break;
            case 3: {

                    if(moveableRight()) {
                        components.get(0).setX(components.get(2).getX());
                        components.get(0).setY(components.get(2).getY() - 1);

                        components.get(1).setX(components.get(2).getX() + 1);
                        components.get(1).setY(components.get(2).getY());

                        components.get(3).setX(components.get(2).getX() - 1);
                        components.get(3).setY(components.get(2).getY());
                        position = 0;
                    }
                    }
                    break;
            default: System.out.println("Greska");
        }
    }

}
