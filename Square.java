public class Square extends Shape {

    public Square() {
        super();
        
        Point p1 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 0);
        Point p2 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 1);
        Point p3 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale) - 1, 0);
        Point p4 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale) - 1, 1);

        components.add(p1);
        components.add(p2);
        components.add(p3);
        components.add(p4);
    }

    @Override
    public void rotate() {
        // TODO

    }
    
}
