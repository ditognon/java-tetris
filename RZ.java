public class RZ extends Shape {
    private int position;
    public RZ() {
        super();

        position = 0;

        Point p1 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 0);
        Point p0 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale) + 1, 0);
        Point p2 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 1);
        Point p3 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale) - 1, 1);

        components.add(p0);
        components.add(p1);
        components.add(p2);
        components.add(p3);
    }

    @Override
    public void rotate() {
        int x2 = components.get(2).getX();
        int y2 = components.get(2).getY();
        switch(position) {
            case 0: {
                    if(moveableDown()) {
                        if(Main.mainArray[y2 - 1][x2 - 1] == 0) {
                            components.get(1).setX(components.get(2).getX() - 1);
                            components.get(1).setY(components.get(2).getY());
        
                            components.get(0).setX(components.get(1).getX());
                            components.get(0).setY(components.get(1).getY() - 1);
        
                            components.get(3).setX(components.get(2).getX());
                            components.get(3).setY(components.get(2).getY() + 1);
        
                            position = 1;
                        }
                    }
                    }
                    break;
            case 1: {
                    if(x2 + 1 < Main.arrayX) {
                        if(Main.mainArray[y2 - 1][x2] == 0 &&
                            Main.mainArray[y2 - 1][x2 + 1] == 0) {
                                components.get(1).setX(components.get(2).getX());
                                components.get(1).setY(components.get(2).getY() - 1);
            
                                components.get(0).setX(components.get(1).getX() + 1);
                                components.get(0).setY(components.get(1).getY());
            
                                components.get(3).setX(components.get(2).getX() - 1);
                                components.get(3).setY(components.get(2).getY());
            
                                position = 0;
                            }
                        }
                    }
                    break;
            default: System.out.println("Greska");
        }
    }
}
