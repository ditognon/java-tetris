public class Line extends Shape {
    private int position;

    public Line() {
        super();
        position = 0;
        Point p0 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 0);
        Point p1 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 1);
        Point p2 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 2);
        Point p3 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 3);

        components.add(p0);
        components.add(p1);
        components.add(p2);
        components.add(p3);
    }

    @Override
    public void rotate() {
        if(position == 0) {
            //Checking the game field boundary
            if((components.get(0).getX() + 2 < Main.arrayX) && (components.get(0).getX() - 1 >= 0)) {
                //Checking intervention with other pieces
                if(Main.mainArray[components.get(1).getY()][components.get(1).getX() + 1] == 0 &&
                    Main.mainArray[components.get(1).getY()][components.get(1).getX() + 2] == 0 &&
                    Main.mainArray[components.get(1).getY()][components.get(1).getX() - 1] == 0) {

                    components.get(0).setX(components.get(1).getX() - 1);
                    components.get(0).setY(components.get(1).getY());

                    components.get(2).setX(components.get(1).getX() + 1);
                    components.get(2).setY(components.get(1).getY());

                    components.get(3).setX(components.get(2).getX() + 1);
                    components.get(3).setY(components.get(2).getY());

                    position = 1;
                }
            }
        }else {
            if(components.get(0).getY() + 2 < Main.arrayY) {
                if(Main.mainArray[components.get(1).getY() + 1][components.get(1).getX()] == 0 &&
                Main.mainArray[components.get(1).getY() + 2][components.get(1).getX()] == 0) {

                    components.get(0).setX(components.get(1).getX());
                    components.get(0).setY(components.get(1).getY() - 1);

                    components.get(2).setX(components.get(1).getX());
                    components.get(2).setY(components.get(1).getY() + 1);

                    components.get(3).setX(components.get(2).getX());
                    components.get(3).setY(components.get(2).getY() + 1);
                    position = 0;
                }
            }
        }

    }
    
}
