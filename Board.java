import javax.swing.JPanel;
import javax.xml.stream.events.EndDocument;

import java.awt.*;

public class Board extends JPanel {
    private Shape activeShape;
    public Board(Shape shape) {
        super();
        this.activeShape = shape;

        this.setRequestFocusEnabled(true);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        //border
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, Main.frameX, Main.frameY);

        //playing field
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(Main.border, Main.border, Main.frameX - 2 * Main.border, Main.frameY - 2 * Main.border);

        //end game border
        g.setColor(Color.CYAN);
        g.fillRect(Main.border, Main.border + Main.scale * Main.endConstant, Main.frameX - 2 * Main.border, Main.scale);

        //placed shapes
        for(int i = 0; i < Main.arrayY; i++) {
            for(int j = 0; j < Main.arrayX; j++) {
                if(Main.mainArray[i][j] != 0) {
                    g.setColor(calculateColor(Main.mainArray[i][j]));
                    g.fillRect(j * Main.scale + Main.scale, i * Main.scale + Main.scale, Main.scale, Main.scale);
                }
            }
        }

        //active shape
        g.setColor(calculateColor(activeShape.getColor()));
        for(Point p : activeShape.points()) {
            g.fillRect(p.getX() * Main.scale  + Main.scale, p.getY() * Main.scale + Main.scale,
             Main.scale, Main.scale);
        }

    }

    public void updateShape(Shape newShape) {
        this.activeShape = newShape;
    }

    private Color calculateColor(int color) {
        switch(color) {
            case 1: return Color.RED;
            case 2: return Color.BLUE;
            case 3: return Color.YELLOW;
            case 4: return Color.DARK_GRAY;
            case 5: return Color.PINK;
            case 6: return Color.GREEN;
            case 7: return Color.ORANGE;
            default: return Color.CYAN;
        }
    }

}
