public class RL extends Shape {
    private int position;
    public RL() {
        super();
        
        position = 0;

        Point p0 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 0);
        Point p1 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 1);
        Point p2 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale), 2);
        Point p3 = new Point((((Main.frameX / 2) - Main.scale) / Main.scale) + 1, 2);

        components.add(p0);
        components.add(p1);
        components.add(p2);
        components.add(p3);
    }

    @Override
    public void rotate() {
        int x1 = components.get(1).getX();
        int y1 = components.get(1).getY();
        switch(position) {
            case 0: {
                    if(x1 - 1 >= 0) {
                        if(Main.mainArray[y1][x1 + 1] == 0 &&
                           Main.mainArray[y1][x1 - 1] == 0 &&
                           Main.mainArray[y1 + 1][x1 - 1] == 0) {
                                components.get(0).setX(components.get(1).getX() + 1);
                                components.get(0).setY(components.get(1).getY());
            
                                components.get(2).setX(components.get(1).getX() - 1);
                                components.get(2).setY(components.get(1).getY());
            
                                components.get(3).setX(components.get(2).getX());
                                components.get(3).setY(components.get(2).getY() + 1);
            
                                position = 1;
                            }
                        }
                    }
                    break;
            case 1: {
                    if(Main.mainArray[y1 + 1][x1] == 0 &&
                       Main.mainArray[y1 - 1][x1] == 0 &&
                       Main.mainArray[y1 - 1][x1 - 1] == 0) {
                            components.get(0).setX(components.get(1).getX());
                            components.get(0).setY(components.get(1).getY() + 1);
        
                            components.get(2).setX(components.get(1).getX());
                            components.get(2).setY(components.get(1).getY() - 1);
        
                            components.get(3).setX(components.get(2).getX() - 1);
                            components.get(3).setY(components.get(2).getY());
        
                            position = 2;
                        }
                    }
                    break;
            case 2: {
                    if(x1 + 1 < Main.arrayX) {
                        if(Main.mainArray[y1][x1 - 1] == 0 &&
                           Main.mainArray[y1][x1 + 1] == 0 &&
                           Main.mainArray[y1 - 1][x1 + 1] == 0) {
                                components.get(0).setX(components.get(1).getX() - 1);
                                components.get(0).setY(components.get(1).getY());
            
                                components.get(2).setX(components.get(1).getX() + 1);
                                components.get(2).setY(components.get(1).getY());
            
                                components.get(3).setX(components.get(2).getX());
                                components.get(3).setY(components.get(2).getY() - 1);
            
                                position = 3;
                            }
                        }
                    }
                    break;
            case 3: {
                    if(moveableDown()) {
                        if(Main.mainArray[y1 - 1][x1] == 0) {
                            components.get(0).setX(components.get(1).getX());
                            components.get(0).setY(components.get(1).getY() - 1);
        
                            components.get(2).setX(components.get(1).getX());
                            components.get(2).setY(components.get(1).getY() + 1);
        
                            components.get(3).setX(components.get(2).getX() + 1);
                            components.get(3).setY(components.get(2).getY());
        
                            position = 0;
                            }
                        }
                    }
                    break;
            default: System.out.println("Greska");
        }

    }
}