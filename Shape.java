import java.util.ArrayList;
import java.util.List;
import java.awt.event.*;

public abstract class Shape {
    protected ArrayList<Point> components;
    protected int color;

    public Shape() {
        components = new ArrayList<Point>();
    }

    public Shape(int color) {
        components = new ArrayList<Point>();
        this.color = color;
    }

    public void clearShape() {
        this.components = new ArrayList<Point>();
    }

    public int getColor() {
        return this.color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void copyShape(Shape shape) {
        this.components.addAll(shape.components);
    }

    public void moveShape() {
        if(Input.getKeyDown(KeyEvent.VK_A) || Input.getKeyDown(KeyEvent.VK_LEFT)) {
            moveLeft();
        }
        if(Input.getKeyDown(KeyEvent.VK_D) || Input.getKeyDown(KeyEvent.VK_RIGHT)) {
            moveRight();
        }
        
        if(Input.getKeyDown(KeyEvent.VK_W) || Input.getKeyDown(KeyEvent.VK_UP)) {
            rotate();
        }

        if(Input.getKeyDown(KeyEvent.VK_S) || Input.getKeyDown(KeyEvent.VK_DOWN)) {
            moveDown();
        }
    }

    public abstract void rotate();
    
    public boolean nextStep() {
        if(moveableDown()) {
            boolean tmp = false;
            for(Point p : components) {
                if(p.getY() + 1 < Main.arrayY) {
                    if(Main.mainArray[p.getY() + 1][p.getX()] != 0) {
                        tmp = true;
                    }
                }
            }

            if(!tmp) {
                moveDown();
            }else {
                for(Point p : components) {
                    Main.mainArray[p.getY()][p.getX()] = color;
                }
            }
            return tmp;
        }else {
            for(Point p : components) {
                Main.mainArray[p.getY()][p.getX()] = color;
            }
            return true;
        }

        
    }

    public List<Point> points() {
        return components;
    }

    public void moveRight() {
        if(moveableRight()) {
            for(Point p : components) {
                p.setX(p.getX() + (Main.step / Main.scale));
            }
        }
    }

    public void moveLeft() {
        if(moveableLeft()) {
            for(Point p : components) {
                p.setX(p.getX() - (Main.step / Main.scale));
            }
        }
    }

    public void moveUp() {
        if(moveableUp()) {
            for(Point p : components) {
                p.setY(p.getY() - (Main.step / Main.scale));
            }
        }
    }

    public boolean moveDown() {
        if(moveableDown()) {
            for(Point p : components) {
                p.setY(p.getY() + (Main.step / Main.scale));
            }
            return true;
        }else {
            return false;
        }
    }

    private boolean pointMoveableRight(Point p) {
        if(p.getX() >= (Main.frameX - Main.border - Main.scale - Main.step) / Main.scale) {
            return false;
        }else if(p.getY() < Main.arrayY - 1 &&
                p.getX() > 0 && p.getX() < Main.arrayX - 1) {
            if(Main.mainArray[p.getY()][p.getX() + 1] != 0) {
            return false;
            }
        }
        return true;
    }

    private boolean pointMoveableLeft(Point p) {
        if(p.getX() < (Main.border) / Main.scale) {
            return false;
        }else if(p.getY() < Main.arrayY - 1 &&
                p.getX() > 0 && p.getX() < Main.arrayX - 1) {
            if(Main.mainArray[p.getY()][p.getX() - 1] != 0) {
                return false;
            }
        }
        return true;
    }

    private boolean pointMoveableDown(Point p) {
        if(p.getY() < (Main.frameY - Main.border - Main.scale - Main.step) / Main.scale) {
            if(Main.mainArray[p.getY() + 1][p.getX()] != 0) {
                return false;
            }else {
                return true;
            }
        }
        return false;
    }

    private boolean pointMoveableUp(Point p) {
        if(p.getY() >= (Main.border + Main.step) / Main.scale) {
            return true;
        }
        return false;
    }

    protected boolean moveableRight() {
        boolean tmp = true;
        for(Point p : components) {
            if(!pointMoveableRight(p)) {
                tmp = false;
            }
        }
        return tmp;
    }

    protected boolean moveableLeft() {
        boolean tmp = true;
        for(Point p : components) {
            if(!pointMoveableLeft(p)) {
                tmp = false;
            }
        }
        return tmp;
    }

    protected boolean moveableUp() {
        boolean tmp = true;
        for(Point p : components) {
            if(!pointMoveableUp(p)) {
                tmp = false;
            }
        }
        return tmp;
    }

    protected boolean moveableDown() {
        boolean tmp = true;
        for(Point p : components) {
            if(!pointMoveableDown(p)) {
                tmp = false;
            }
        }
        return tmp;
    }
}

