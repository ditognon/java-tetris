# Java Tetris

Simple tetris game made in java for fun. Not yet completed!

## Running the game

To run the game simply run Main.java. Game window pops up and the game automatically starts.
You can move the shape left and right with left and right arrow key respectively. Down arrow key speeds up the shape, up arrow key rotates the shape! Number of clear rows(score) is written in the console!

## State of the game

The game is not yet finished, and it may never be. The plan is to make a start menu which would contain a leaderboard with previous scores, and settings in which the player could set shape speed, frame size, and some other things.
