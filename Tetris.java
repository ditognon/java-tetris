import java.util.Stack;

public class Tetris {
    private Shape activeShape;

    public Tetris(Shape activeShape) {
        this.activeShape = activeShape;
    }

    public void updateShape(Shape activeShape) {
        this.activeShape = activeShape;
    }

    //Moves the active shape one move down, returns true if the shape
    //can't go further down
    public boolean nextStep() {
        return activeShape.nextStep();
    }

    //Checks for full rows and clears them
    public void clearRows() {
        boolean fullRow;
        int tmp;
        for(int i = 0; i < Main.arrayY; i++) {
            fullRow = true;
            for(int j = 0; j < Main.arrayX; j++) {
                if(Main.mainArray[i][j] == 0) {
                    fullRow = false;
                }
            }

            if(fullRow) {
                Main.points++;
                for(int ii = i; ii > 0; ii--) {
                    for(int jj = 0; jj < Main.arrayX; jj++) {
                        Main.mainArray[ii][jj] = Main.mainArray[ii - 1][jj];
                    }
                }
            }
        }
    }

    public boolean checkEndGame() {
        boolean tmp = false;
        for(int j = 0; j < Main.arrayX; j++) {
            if(Main.mainArray[Main.endConstant][j] != 0) tmp = true;
        }

        return tmp;
    }

}
