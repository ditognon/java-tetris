import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Main {
    private static int delay = 100;
    public static int step = 10;
    public static int points = 0;
    public final static int scale = 10;
    public final static int endConstant = 4;
    public final static int border = 10;
    public final static int frameX = 220;
    public final static int frameY = 320;
    public static int mainArray[][];
    public final static int arrayX = (frameX - 2 * border) / 10;
    public final static int arrayY = (frameY - 2 * border) / 10;
    private static Board board;
    public static Input input;
    private static Window window;
    private static Shape activeShape;
    private static ShapeGenerator shapeGenerator;

    public static void main(String args[]) {
        shapeGenerator = new ShapeGenerator();
        mainArray = new int[arrayY][arrayX];
        activeShape = shapeGenerator.generateRandom();
        Tetris tetris = new Tetris(activeShape);
        input = new Input();
        input.init();
        board = new Board(activeShape);
        window = new Window(board, input);
        window.setVisible(true);
        window.setSize(600, 800);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(tetris.nextStep()) {
                    tetris.clearRows();
                    activeShape.clearShape();

                    activeShape = shapeGenerator.generateRandom();
                    tetris.updateShape(activeShape);
                    board.updateShape(activeShape);

                    System.out.println("Your points: " + points);
                }

                if(tetris.checkEndGame()) {
                    System.out.println("Izgubili ste!");
                    System.exit(0);
                }
            
                activeShape.moveShape();

                SwingUtilities.invokeLater(() -> {
                    board.repaint();
                });
                
            }
        }).start();
    }

}
