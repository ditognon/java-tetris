import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.*;

public class Window extends JFrame {
    private Board board;
    private Input input;
    private Button startButton;
    private Button endButton;
    private Button leaderBoardButton;

    public Window(Board board, Input input) {
        super("Game1");
        this.board = board;
        this.addKeyListener(input);

/*         this.startButton = new Button("Start");
        this.endButton = new Button("End");
        this.leaderBoardButton = new Button("leaderBoard");

        JPanel buttons = new JPanel();
        buttons.add(startButton);
        buttons.add(endButton);
        buttons.add(leaderBoardButton); */

        //this.add(buttons, BorderLayout.SOUTH);
        this.add(board, BorderLayout.CENTER);
    }
}
